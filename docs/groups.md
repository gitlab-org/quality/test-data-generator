[Back](../README.md)

# Groups

The Test Data Generator will create a single parent group that will contain all the group level assets that will be created.
The group will be created as a public group and with a name specified via config.

[Projects](./projects.md), [Snippets](./snippets.md) and [Wikis](./wikis.md) can be created at the group level. The amount of
Snippets and Wikis to create should be specified with the groups configuration

## Configuration

```yaml
parent_group:
  name: test-data
  Assets:
    # Assets Config
```

- `name`: Name of the parent group to create. All projects will also be created under this parent group.
- `Assets`: Contains the configuration to specify the amount of Snippets/Wikis to create.
