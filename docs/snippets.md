[Back](../README.md)

# Snippets

Snippets can be generated at the project or instance level.

## Configuration

The configuration for snippets can be set in 2 different places depending on if you're wanting to create instance level snippets, 
or project level snippets.

```yaml
parent_group:
  assets:
    snippets:
      # Specific amount
      items_to_create: 10
      # Random amount
      min_items_to_create: 5
      max_items_to_create: 15

project:
  assets:
    snippets:
      # Specific amount
      items_to_create: 30
      # Random amount
      min_items_to_create: 5
      max_items_to_create: 15
```
