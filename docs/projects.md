[Back](../README.md)

# Projects

Projects are the main resource created by the Test Data Generator with most other assets being created as part of a project.
As such most configuration lives under the project section. The number of projects to create is specified as part of the configuration
and all projects will be created under the parent [group](./groups.md).

## Configuration

```yaml
project:
  count: 5000
  repository:
    # Repository Config
  assets:
    # Assets Config
```

- `count`: The number of projects to create.
- `repository`: Contains the configuration for how each projects repository should be created.
- `assets`: Contains the configuration to specify how many of each different asset type should be created.
