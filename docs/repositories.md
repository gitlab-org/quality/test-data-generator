[Back](../README.md)

# Repositories

A repository can be created as part of each project. This will generate a repository structure that can contain multiple folders and
file types. All folder and file names are randomly generated strings. The amount of folders created can be randomly generated or a
specific number can be set. When creating directories, the Test Data Generator will either create a folder at the root level, or
it will nest the folder inside another created folder. This allows a more complex repository structure to be created.

## Configuration

The configuration for repositories should be placed below the existing project configuration.

```yaml
project:
  repository:
    create: true
    dirs:
      # Specific amount
      items_to_create: 8
      # Random amount
      min_items_to_create: 15
      max_items_to_create: 75
    files_per_dir:
      # Specific amount
      items_to_create: 10
      # Random amount
      min_items_to_create: 5
      max_items_to_create: 15
    lfs:
      min_items_to_create: 1
      max_items_to_create: 10
```

- `create`: Specify if projects will be created with a repository. Defaults to `false`.
- `dirs`: Specify how many directories should be created as part of the repository.
- `files_per_dir`: Specify how many files will be created inside each created directory.
- `lfs`: Specify how many [LFS](https://docs.gitlab.com/ee/topics/git/lfs/) files will be created inside each created directory.

### Extra Options

There are extra options available that tweak the standard behavior. Adding the below to your config file will allow customization:

```yml
extra_options:
  lfs:
    file_size_mb: 10
```

- `file_size_mb` - specify file size (Mb) to be uploaded in comments. When this option is omitted, by default the file sizes are 2 Mb.
