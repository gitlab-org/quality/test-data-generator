[Back](../README.md)

# Wikis

Wikis can be generated at the project or instance level.

## Configuration

The configuration for wikis can be set in 2 different places depending on if you're wanting to create a group level wiki,
or project level wiki.

```yaml
parent_group:
  assets:
    wikis:
      # Specific amount
      items_to_create: 10
      # Random amount
      min_items_to_create: 5
      max_items_to_create: 15

project:
  assets:
    wikis:
      # Specific amount
      items_to_create: 30
      # Random amount
      min_items_to_create: 5
      max_items_to_create: 15
```
