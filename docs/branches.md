[Back](../README.md)

# Branches

Branches are created as part of a repository and each branch will contain a single new file.

## Configuration

The configuration for branches should be placed below the existing repository configuration.

```yaml
project:
  repository:
    create: true
    branches:
      # Specific amount
      items_to_create: 5
      # Random amount
      min_items_to_create: 3
      max_items_to_create: 8
```

## Merge Requests

Each new branch created will automatically create a new merge request at the same time. As such there is no specific configuration
for merge requests that needs to be added.
