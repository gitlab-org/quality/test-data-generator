[Back](../README.md)

# Issues

Issues are created under each project. Each issue created will contain random amounts of text and have a random name.
Some comments will include image or TXT file upload.

## Configuration

The configuration for issues should be placed below the assets section for the project configuration.

```yaml
project:
  assets:
    issues:
      # Specific amount
      items_to_create: 50
      # Random amount
      min_items_to_create: 25
      max_items_to_create: 100
```

## Comments

Each issue created will also contain a random amount of comments on the issues. The amount of comments created will be randomly
selected between 3 - 25.

### Extra Options

There are extra options available that tweak comments behavior. Adding the below to your config file will allow customization:

```yml
extra_options:
  comments:
    comments_only_file_upload: true
    upload_file_size_mb: 10
```

- `comments_only_file_upload` - all comments will include uploaded file. Default is `false`.
- `upload_file_size_mb` - specify file size (Mb) to be uploaded in comments. When this option is omitted, by default the file sizes are 2 Mb.
