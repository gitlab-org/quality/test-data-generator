[Back](../README.md)

# Pipelines

A CI file can be added to each project. The CI file is designed to randomly pass/fail and have an asset available for download.
For the CI file to pass the test environment being used will need to have a runner configured to allow the jobs to run.

## Configuration

The configuration for pipelines should be placed as part of the repositories' configuration.

```yaml
project:
  repository:
    create: true
    cicd: true
```

## .gitlab-ci.yml

The below `.gitlab-ci.yml` file will be placed into each project if `cicd` is set to `true`:

```yaml
image: busybox:latest

before_script:
  - echo "Before script section"
  - echo "For example you might run an update here or install a build dependency"
  - echo "Or perhaps you might print out some debugging details"

after_script:
  - echo "After script section"
  - echo "For example you might do some cleanup here"

build1:
  stage: build
  script:
    - echo "Do your build here"

test1:
  stage: test
  script:
    - echo "Do a test here"
    - echo "For example run a test suite"

test2:
  stage: test
  script:
    - echo "Do another parallel test here"
    - echo "For example run a lint test"

deploy1:
  stage: deploy
  script:
    - touch test.txt
    - exit $(($RANDOM % 2))
  artifacts:
    paths:
      - ./**/*.txt
  environment: production
```
