package main

import (
	"context"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/alecthomas/kingpin/v2"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"gitlab.com/gitlab-org/quality/test-data-generator/pkg/gitlab"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sigs
		cancel()
	}()

	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = level.NewInjector(logger, level.InfoValue())

	args := &gitlab.Args{
		Context: ctx,
		Logger:  logger,
	}

	if kingpin.Parse() == "create" {
		fatal(logger, createData(args))
	}
}

func fatal(logger log.Logger, err error) {
	if err != nil {
		level.Error(logger).Log("event", "shutdown", "error", err)
		if strings.Contains(err.Error(), "context canceled") {
			os.Exit(0)
		}
		os.Exit(1)
	}
}
