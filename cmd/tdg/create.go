package main

import (
	"os"
	"strings"
	"sync"
	"time"

	"github.com/alecthomas/kingpin/v2"
	"github.com/fatih/semgroup"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
	tdg "gitlab.com/gitlab-org/quality/test-data-generator"
	"gitlab.com/gitlab-org/quality/test-data-generator/pkg/gitlab"
	"golang.org/x/time/rate"
)

var (
	createCmd       = kingpin.Command("create", "createData test data.")
	createCmdConfig = createCmd.Flag("config", "Path to config file").
			Envar("TDG_CONFIG").Required().String()
	createCmdAPIToken = createCmd.Flag("gitlab-token", "Admin API token").
				Envar("TDG_API_TOKEN").Required().String()
	createCmdGitLabURL = createCmd.Flag("gitlab-url", "URL for the GitLab instance to seed.").
				Envar("TDG_GITLAB_URL").Required().String()
	createCmdGitLabRegistryURL = createCmd.Flag("gitlab-registry-url", "Registry URL for the GitLab instance to seed.").
					Envar("TDG_GITLAB_REGISTRY_URL").String()
)

func createData(args *gitlab.Args) error {
	data, err := os.ReadFile(*createCmdConfig)
	if err != nil {
		return err
	}

	cfg, err := gitlab.LoadConfig(data, *createCmdGitLabURL, *createCmdGitLabRegistryURL)
	if err != nil {
		return err
	}

	gitlabClient, err := goGitLab.NewClient(
		*createCmdAPIToken,
		goGitLab.WithBaseURL(cfg.GitLab.APIURL),
		goGitLab.WithCustomLimiter(rate.NewLimiter(5000, 5000)),
	)
	if err != nil {
		return err
	}

	args.GitLabClient = gitlabClient
	args.Config = cfg

	go gitlab.MonitorEnvironmentHealth(args.Config.GitLab.URL)

	if args.Config.Project.Assets.Containers.ItemsToCreate > 0 || args.Config.Project.Assets.Containers.MaxItemsToCreate > 0 {
		err = gitlab.DockerLogin(args, *createCmdAPIToken)
		if err != nil {
			level.Error(args.Logger).Log("event", "dockerLogin", "msg", "failed to login to docker", "error", err)
		}
	}

	parentGroup, err := createParentGroup(args)
	if err != nil {
		return err
	}

	createGroupAssets(args, parentGroup)

	err = createProject(args, parentGroup, createCmdAPIToken)
	if err != nil {
		return err
	}

	return nil
}

func createParentGroup(args *gitlab.Args) (*goGitLab.Group, error) {
	parentGroup, err := gitlab.GetGroupByName(args)
	if err != nil {
		return &goGitLab.Group{}, err
	}

	if parentGroup.ID > 0 {
		level.Warn(args.Logger).Log("event", "createParentGroup", "group_name", args.Config.ParentGroup.Name, "msg", "found existing group")
		time.Sleep(5 * time.Second)
	} else {
		parentGroup, err = gitlab.CreateGroup(args)
		if err != nil {
			return &goGitLab.Group{}, err
		}
	}

	return parentGroup, nil
}

func createProject(args *gitlab.Args, parentGroup *goGitLab.Group, apiToken *string) error {
	const (
		oneMillion = 1000000
	)

	sg := semgroup.NewGroup(args.Context, args.Config.Concurrencies.Projects)
	startTime := time.Now()

	if args.Config.Project.RunTime != time.Second {
		args.Config.Project.Count = oneMillion
	}

	for i := 0; i < args.Config.Project.Count; i++ {
		if time.Since(startTime) >= args.Config.Project.RunTime {
			break
		}

		select {
		case <-args.Context.Done():
			return args.Context.Err()
		default:
			sg.Go(func() error {
				project, err := gitlab.CreateProject(args, parentGroup)
				if err != nil {
					return err
				}

				var wg sync.WaitGroup
				wg.Add(1)
				go func() {
					tdg.ProcessJobsConcurrently(gitlab.CreateProjectSnippet(args.Context, args, project))
					gitlab.CreateProjectWiki(args, project)
					gitlab.CreateIssue(args, project)
					gitlab.UploadDockerContainer(args, project)
					gitlab.UploadPackage(args, project, *apiToken)
					tdg.ProcessJobsConcurrently(gitlab.CreateProjectMilestone(args.Context, args, project))
					wg.Done()
				}()

				err = gitlab.CreateRepository(args, project, *createCmdAPIToken)
				if err != nil {
					wg.Wait()
					return err
				}

				gitlab.CreateBranch(args, project)
				wg.Wait()
				return nil
			})
		}
	}

	if err := sg.Wait(); err != nil {
		if contextCanceled(args.Logger, err) {
			return args.Context.Err()
		}
		return err
	}

	return nil
}

func createGroupAssets(args *gitlab.Args, group *goGitLab.Group) {
	tdg.ProcessJobsConcurrently(gitlab.CreateSnippet(args.Context, args))
	gitlab.CreateGroupWiki(args, group)
	tdg.ProcessJobsConcurrently(gitlab.CreateGroupMilestone(args.Context, args, group))
}

func contextCanceled(logger log.Logger, err error) bool {
	if strings.Contains(err.Error(), "context canceled") {
		level.Warn(logger).Log("event", "context cancelled", "msg", "Skipped waiting for project creation due to context cancelled.")
		return true
	}
	return false
}
