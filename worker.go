package tdg

import (
	"fmt"
	"runtime"
	"sync"
)

func ProcessJobs(data []func() error) {
	processJobs(data, 1)
}

func ProcessJobsConcurrently(data []func() error) {
	processJobs(data, runtime.NumCPU()/2)
}

func processJobs(data []func() error, poolSize int) {
	var wg sync.WaitGroup
	requests := make(chan func() error, poolSize)

	for i := 0; i < poolSize; i++ {
		go worker(requests, &wg)
	}

	for _, s := range data {
		wg.Add(1)
		requests <- s
	}
	wg.Wait()
	close(requests)
}

func worker(requests <-chan func() error, wg *sync.WaitGroup) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("worker panicked:", r)
		}
	}()
	for req := range requests {

		if err := req(); err != nil {
			fmt.Println(err)
		}

		wg.Done()
	}
}
