FROM golang:1.21.1-alpine

WORKDIR $GOPATH/src/gitlab.com/gitlab-org/quality/test-data-generator

COPY . .
RUN go mod download
RUN go build ./cmd/tdg

CMD [ "bash" ]