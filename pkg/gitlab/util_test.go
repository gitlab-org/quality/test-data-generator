package gitlab

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type testGenerateRandomWordsOptions struct {
	name          string
	numberOfWords int
	spacer        string
	expectedWords int
}

func Test_generateRandomWords(t *testing.T) {
	tests := []testGenerateRandomWordsOptions{
		{
			name:          "NumberOfWordsTwoReturnsTwoWords",
			numberOfWords: 2,
			spacer:        " ",
			expectedWords: 2,
		},
		{
			name:          "ZeroNumberOfWordsReturnsNoWord",
			numberOfWords: 0,
			spacer:        "",
			expectedWords: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := generateRandomWords(tt.numberOfWords, tt.spacer)
			require.Len(t, strings.Split(s, tt.spacer), tt.expectedWords)
		})
	}
}

func Benchmark_generateRandomWords(b *testing.B) {
	tests := []testGenerateRandomWordsOptions{
		{
			name:          "GenerateOneWord",
			numberOfWords: 1,
			spacer:        " ",
		},
		{
			name:          "GenerateTenWords",
			numberOfWords: 10,
			spacer:        " ",
		},
		{
			name:          "GenerateOneHundredWords",
			numberOfWords: 100,
			spacer:        " ",
		},
		{
			name:          "GenerateOneThousandWords",
			numberOfWords: 1000,
			spacer:        " ",
		},
		{
			name:          "GenerateTenThousandWords",
			numberOfWords: 10000,
			spacer:        " ",
		},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				generateRandomWords(tt.numberOfWords, tt.spacer)
			}
		})
	}
}

func Test_generateFileContent_ReturnsNumberOfLinesBetweenMinMax(t *testing.T) {
	const (
		testMinNumberOfLines = 40
		testMaxNumberOfLines = 80
	)

	for i := 0; i < 1000; i++ {
		s := generateFileContent(testMinNumberOfLines, testMaxNumberOfLines)
		lineCount := len(strings.Split(s, "\n"))
		greaterThan := lineCount >= testMinNumberOfLines
		lessThan := lineCount <= testMaxNumberOfLines

		require.True(t, greaterThan)
		require.True(t, lessThan)
	}
}

type testGenerateFileContentOptions struct {
	name             string
	minNumberOfLines int
	maxNumberOfLines int
	expectedText     string
}

func Test_generateFileContent(t *testing.T) {
	tests := []testGenerateFileContentOptions{
		{
			name:             "ReturnsErrorWhenZeroNumberOfLines",
			expectedText:     "Max number of lines cannot be zero",
			minNumberOfLines: 0,
			maxNumberOfLines: 0,
		},
		{
			name:             "ReturnsErrorWhenMinNumberOfLinesIsGreaterThanMaxNumberOfLines",
			expectedText:     "Min number of lines cannot be greater than max number of lines",
			minNumberOfLines: 2,
			maxNumberOfLines: 1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := generateFileContent(tt.minNumberOfLines, tt.maxNumberOfLines)
			require.Equal(t, tt.expectedText, s)
		})
	}
}

func Benchmark_generateFileContent(b *testing.B) {
	tests := []testGenerateFileContentOptions{
		{
			name:             "GenerateBetweenFortyAndOneHundredLines",
			minNumberOfLines: 40,
			maxNumberOfLines: 100,
		},
		{
			name:             "GenerateBetweenOneHundredAndOneThousandLines",
			minNumberOfLines: 100,
			maxNumberOfLines: 1000,
		},
		{
			name:             "GenerateBetweenOneThousandAndTenThousandLines",
			minNumberOfLines: 1000,
			maxNumberOfLines: 10000,
		},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				generateFileContent(tt.minNumberOfLines, tt.maxNumberOfLines)
			}
		})
	}
}

func Test_generateLargeFileData_ReturnsByteSizeBetweenOneAndFive(t *testing.T) {
	for i := 0; i < 1000; i++ {
		b := generateLargeFileData(0)
		byteSize := len(b)
		greaterThan := byteSize >= oneMB
		lessThan := byteSize <= fiveMB

		require.True(t, greaterThan)
		require.True(t, lessThan)
	}
}

func Test_generateLargeFileData_ReturnsByteSizeEqualToLargeFileDataSize(t *testing.T) {
	const (
		eightMB = 8388608
	)

	b := generateLargeFileData(8)
	require.Len(t, b, eightMB)
}

type testGenerateLargeFileDataOptions struct {
	name              string
	largeFileDataSize int
}

func Benchmark_generateLargeFileData(b *testing.B) {
	tests := []testGenerateLargeFileDataOptions{
		{
			name:              "GenerateOneToFiveMB",
			largeFileDataSize: 0,
		},
		{
			name:              "GenerateTenMB",
			largeFileDataSize: 10,
		},
		{
			name:              "GenerateOneHundredMB",
			largeFileDataSize: 100,
		},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				generateLargeFileData(tt.largeFileDataSize)
			}
		})
	}
}

type testRandomStringOptions struct {
	name         string
	stringLength int
}

func Test_randomString(t *testing.T) {
	tests := []testRandomStringOptions{
		{
			name:         "ReturnsStringLengthZeroWhenPassedZero",
			stringLength: 0,
		},
		{
			name:         "ReturnsStringLengthTenWhenPassedTen",
			stringLength: 10,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := randomString(tt.stringLength)
			require.Len(t, s, tt.stringLength)
		})
	}
}

func Test_randomString_OnlyAllowedCharactersAreReturned(t *testing.T) {
	const (
		stringLength = 1000
	)

	s := randomString(stringLength)
	for _, v := range strings.Split(s, "") {
		assert.True(t, strings.Contains(allowedChars, v))
	}
}

func Benchmark_randomString(b *testing.B) {
	tests := []testRandomStringOptions{
		{
			name:         "GenerateTenCharacterLengthWord",
			stringLength: 10,
		},
		{
			name:         "GenerateOneHundredCharacterLengthWord",
			stringLength: 100,
		},
		{
			name:         "GenerateOneThousandCharacterLengthWord",
			stringLength: 1000,
		},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				randomString(tt.stringLength)
			}
		})
	}
}

type testItemsToCreateOptions struct {
	name     string
	counts   countConfig
	expected int
}

func Test_itemsToCreate(t *testing.T) {
	tests := []testItemsToCreateOptions{
		{
			name: "ReturnsZeroWhenNeitherItemsOrMaxSpecified",
			counts: countConfig{
				ItemsToCreate:    0,
				MinItemsToCreate: 3,
				MaxItemsToCreate: 0,
			},
			expected: 0,
		},
		{
			name: "ReturnsItemsToCreateWhenSpecified",
			counts: countConfig{
				ItemsToCreate:    5,
				MinItemsToCreate: 3,
				MaxItemsToCreate: 10,
			},
			expected: 5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := itemsToCreate(tt.counts)
			require.Equal(t, tt.expected, n)
		})
	}
}

func Test_itemsToCreate_ReturnsNumberBetweenMinAndMaxWhenSpecified(t *testing.T) {
	counts := countConfig{
		ItemsToCreate:    0,
		MinItemsToCreate: 3,
		MaxItemsToCreate: 10,
	}

	for i := 0; i < 1000; i++ {
		n := itemsToCreate(counts)
		greaterThan := n >= counts.MinItemsToCreate
		lessThan := n <= counts.MaxItemsToCreate

		require.True(t, greaterThan)
		require.True(t, lessThan)
	}
}
