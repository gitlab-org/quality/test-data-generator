package gitlab

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

type ProjectCfg struct {
	Count      int           `yaml:"count"`
	RunTime    time.Duration `yaml:"run_time"`
	Repository RepositoryCfg `yaml:"repository"`
	Assets     assetsConfig  `yaml:"assets"`
}

var (
	visibilities = []goGitLab.VisibilityValue{"private", "internal", "public"}
)

const (
	projectTitleLength = 3
)

func CreateProject(args *Args, parentGroup *goGitLab.Group) (*goGitLab.Project, error) {
	waitForEnvironmentHealthy()

	projectName := fmt.Sprintf("%v-project-%v", strings.ReplaceAll(args.Config.ParentGroup.Name, "/", "-"), generateRandomWords(projectTitleLength, "-"))

	opts := goGitLab.CreateProjectOptions{
		Name:                       goGitLab.String(projectName),
		NamespaceID:                &parentGroup.ID,
		Description:                goGitLab.String("Created via Test Data Generator"),
		Visibility:                 goGitLab.Visibility(visibilities[rand.Intn(len(visibilities))]),
		InitializeWithReadme:       &args.Config.Project.Repository.Create,
		AutoCancelPendingPipelines: goGitLab.String("disabled"),
	}

	level.Info(args.Logger).Log("event", "CreateProject", "project_name", projectName, "msg", "creating new project")
	project, _, err := args.GitLabClient.Projects.CreateProject(&opts, goGitLab.WithContext(args.Context))
	if err != nil {
		level.Error(args.Logger).Log("event", "CreateProject", "project_name", projectName, "msg", "failed to create project", "error", err)
		return nil, err
	}

	level.Info(args.Logger).Log("event", "CreateProject", "project_name", projectName, "project_weburl", project.WebURL, "msg", "created new project")
	return project, nil
}
