package gitlab

import (
	"fmt"
	"os/exec"

	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	containerNameLength = 3
)

func UploadDockerContainer(args *Args, project *goGitLab.Project) {
	level.Info(args.Logger).Log("event", "UploadContainer", "project_weburl", project.WebURL)
	for i := 0; i < itemsToCreate(args.Config.Project.Assets.Containers); i++ {
		waitForEnvironmentHealthy()

		containerName := fmt.Sprintf("container-%v", generateRandomWords(containerNameLength, "-"))
		containerTag := fmt.Sprintf("%v:%v", project.ContainerRegistryImagePrefix, containerName)

		level.Info(args.Logger).Log("event", "dockerBuild", "project_weburl", project.WebURL)
		buildCmd := exec.Command("docker", "build", "-t", containerTag, "./assets/docker")
		_, buildErr := buildCmd.Output()
		if buildErr != nil {
			level.Error(args.Logger).Log("event", "dockerBuild", "project_weburl", project.WebURL,
				"msg", "failed to build container", "error", buildErr)
			continue
		}

		level.Info(args.Logger).Log("event", "dockerPush", "project_weburl", project.WebURL)
		pushCmd := exec.Command("docker", "push", containerTag)
		_, pushErr := pushCmd.Output()
		if pushErr != nil {
			level.Error(args.Logger).Log("event", "dockerPush", "project_weburl", project.WebURL,
				"msg", "failed to push container", "error", pushErr)
			continue
		}

		level.Info(args.Logger).Log("event", "UploadContainer", "project_weburl", project.WebURL, "msg", "Container Uploaded")
	}
}

func DockerLogin(args *Args, apiToken string) error {
	level.Info(args.Logger).Log("event", "dockerLogin")
	loginCmd := exec.Command("docker", "login", "--username", "root", "--password",
		apiToken, args.Config.GitLab.ContainerRegistryURL)
	_, err := loginCmd.Output()
	return err
}
