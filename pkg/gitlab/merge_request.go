package gitlab

import (
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	mergeRequestMaxNumberOfLines = 100
	mergeRequestMinNumberOfLines = 60
	mergeRequestNameLength       = 3
	targetBranchName             = "main"
)

func CreateMergeRequest(args *Args, project *goGitLab.Project, branchName string) {
	waitForEnvironmentHealthy()

	level.Info(args.Logger).Log("event", "CreateMergeRequest", "project_weburl", project.WebURL)
	mergeRequestName := generateRandomWords(mergeRequestNameLength, " ")
	opts := goGitLab.CreateMergeRequestOptions{
		SourceBranch: goGitLab.String(branchName),
		TargetBranch: goGitLab.String(targetBranchName),
		Title:        goGitLab.String(mergeRequestName),
		Description:  goGitLab.String(generateFileContent(mergeRequestMinNumberOfLines, mergeRequestMaxNumberOfLines)),
	}

	_, _, err := args.GitLabClient.MergeRequests.CreateMergeRequest(project.ID, &opts, goGitLab.WithContext(args.Context))
	if err != nil {
		level.Error(args.Logger).Log("event", "CreateMergeRequest", "project_weburl", project.WebURL,
			"merge_request_name", mergeRequestName, "msg", "failed to create merge request", "error", err)
		return
	}

	level.Info(args.Logger).Log("event", "CreateMergeRequest", "project_weburl", project.WebURL,
		"merge_request_name", mergeRequestName, "msg", "merge request created")
}
