package gitlab

import (
	"fmt"

	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

type ParentGroupCfg struct {
	Name   string       `yaml:"name"`
	Assets assetsConfig `yaml:"assets"`
}

func GetGroupByName(args *Args) (*goGitLab.Group, error) {
	waitForEnvironmentHealthy()

	parentGroup := &goGitLab.Group{}
	groups, _, err := args.GitLabClient.Groups.SearchGroup(args.Config.ParentGroup.Name, goGitLab.WithContext(args.Context))
	if err != nil {
		level.Error(args.Logger).Log("event", "GetGroupByName", "group_name", args.Config.ParentGroup.Name,
			"msg", "failed searching for group", "error", err)
		return &goGitLab.Group{}, err
	}

	if len(groups) > 1 {
		level.Error(args.Logger).Log("event", "GetGroupByName", "group_name", args.Config.ParentGroup.Name,
			"msg", fmt.Errorf("found %d groups expected 1 or 0", len(groups)))
		return &goGitLab.Group{}, fmt.Errorf("search groups: found %d groups expected 1 or 0", len(groups))
	} else if len(groups) == 1 {
		parentGroup = groups[0]
	}

	return parentGroup, nil
}

func CreateGroup(args *Args) (*goGitLab.Group, error) {
	opts := goGitLab.CreateGroupOptions{
		Name:        &args.Config.ParentGroup.Name,
		Path:        &args.Config.ParentGroup.Name,
		Description: goGitLab.String("Created via Test Data Generator"),
		Visibility:  goGitLab.Visibility("public"),
	}

	level.Info(args.Logger).Log("event", "CreateGroup", "group_name", args.Config.ParentGroup.Name, "msg", "creating new group")
	group, _, err := args.GitLabClient.Groups.CreateGroup(&opts, goGitLab.WithContext(args.Context))
	if err != nil {
		level.Error(args.Logger).Log("event", "CreateGroup", "group_name", args.Config.ParentGroup.Name,
			"msg", "Failed to create new group")
		return &goGitLab.Group{}, err
	}

	return group, nil
}
