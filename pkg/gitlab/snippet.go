package gitlab

import (
	"context"
	"fmt"
	"math/rand"

	"github.com/go-kit/log/level"
	"github.com/pkg/errors"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	snippetFileNameLength = 2
	snippetNameLength     = 3
)

func CreateSnippet(ctx context.Context, args *Args) []func() error {
	return createSnippet(ctx, args, args.Config.ParentGroup.Assets.Snippets)
}

func CreateProjectSnippet(ctx context.Context, args *Args, project *goGitLab.Project) []func() error {
	return createProjectSnippet(ctx, args, args.Config.Project.Assets.Snippets, project)
}

func createSnippet(ctx context.Context, args *Args, snippetCfg countConfig) []func() error {
	snippetsToCreate := itemsToCreate(snippetCfg)
	level.Info(args.Logger).Log("snippet_count", snippetsToCreate)

	var snippets []func() error

	for i := 0; i < snippetsToCreate; i++ {
		snippetName := fmt.Sprintf("snippet-%v", generateRandomWords(snippetNameLength, "-"))
		snippetFileName := fmt.Sprintf("%v.%v", generateRandomWords(snippetFileNameLength, "-"), fileExtensions[rand.Intn(len(fileExtensions))])
		snippetFileOptions := &[]*goGitLab.CreateSnippetFileOptions{{
			FilePath: goGitLab.String(snippetFileName),
			Content:  goGitLab.String(generateFileContent(fileContentMinNumberOfLines, fileContentMaxNumberOfLines)),
		}}

		snippets = append(snippets, func() error {
			snippet, _, err := args.GitLabClient.Snippets.CreateSnippet(&goGitLab.CreateSnippetOptions{
				Title:       goGitLab.String(snippetName),
				Description: goGitLab.String("Created via Test Data Generator"),
				Visibility:  goGitLab.Visibility(visibilities[rand.Intn(len(visibilities))]),
				Files:       snippetFileOptions,
			}, goGitLab.WithContext(ctx))
			if err != nil {
				level.Error(args.Logger).Log(
					"snippet_name", snippetName,
					"filename", snippetFileName,
					"msg", "failed to create snippet",
					"error", err,
				)
				return errors.Wrap(err, "error creating snippet")
			}
			level.Info(args.Logger).Log(
				"snippet_name", snippetName,
				"filename", snippetFileName,
				"snippet_weburl", snippet.WebURL,
				"msg", "snippet created",
			)
			return nil
		})
	}

	return snippets
}

func createProjectSnippet(ctx context.Context, args *Args, snippetCfg countConfig, project *goGitLab.Project) []func() error {
	snippetsToCreate := itemsToCreate(snippetCfg)
	level.Info(args.Logger).Log("project_snippet_count", snippetsToCreate)

	var snippets []func() error

	for i := 0; i < snippetsToCreate; i++ {
		snippetName := fmt.Sprintf("snippet-%v", generateRandomWords(snippetNameLength, "-"))
		snippetFileName := fmt.Sprintf("%v.%v", generateRandomWords(snippetFileNameLength, "-"), fileExtensions[rand.Intn(len(fileExtensions))])
		snippetFileOptions := &[]*goGitLab.CreateSnippetFileOptions{{
			FilePath: goGitLab.String(snippetFileName),
			Content:  goGitLab.String(generateFileContent(fileContentMinNumberOfLines, fileContentMaxNumberOfLines)),
		}}

		snippets = append(snippets, func() error {
			snippet, _, err := args.GitLabClient.ProjectSnippets.CreateSnippet(project.ID, &goGitLab.CreateProjectSnippetOptions{
				Title:       goGitLab.String(snippetName),
				Description: goGitLab.String("Created via Test Data Generator"),
				Visibility:  goGitLab.Visibility(visibilities[rand.Intn(len(visibilities))]),
				Files:       snippetFileOptions,
			}, goGitLab.WithContext(ctx))
			if err != nil {
				level.Error(args.Logger).Log(
					"snippet_name", snippetName,
					"filename", snippetFileName,
					"snippet_project", project.WebURL,
					"msg", "failed to create project snippet",
					"error", err,
				)
				return errors.Wrap(err, "error creating project snippet")
			}
			level.Info(args.Logger).Log(
				"snippet_name", snippetName,
				"filename", snippetFileName,
				"snippet_weburl", snippet.WebURL,
				"msg", "project snippet created",
			)
			return nil
		})
	}

	return snippets
}
