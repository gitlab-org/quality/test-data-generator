package gitlab

import (
	"fmt"

	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	issueTitleLength      = 3
	labelLength           = 1
	maxLabelsToAppend     = 8
	issueMaxNumberOfLines = 100
	issueMinNumberOfLines = 60
	minLabelsToAppend     = 1
)

func CreateIssue(args *Args, project *goGitLab.Project) {
	issueCount := itemsToCreate(args.Config.Project.Assets.Issues)
	level.Info(args.Logger).Log("event", "CreateIssue", "issue_count", issueCount, "project_weburl", project.WebURL)
	labelCounts := countConfig{
		ItemsToCreate:    0,
		MinItemsToCreate: minLabelsToAppend,
		MaxItemsToCreate: maxLabelsToAppend,
	}

	for i := 0; i < issueCount; i++ {
		waitForEnvironmentHealthy()

		issueTitle := fmt.Sprintf("issue-%v", generateRandomWords(issueTitleLength, "-"))
		issueContent := generateFileContent(issueMinNumberOfLines, issueMaxNumberOfLines)
		var issueLabels goGitLab.Labels

		for j := 0; j < itemsToCreate(labelCounts); j++ {
			issueLabels = append(issueLabels, generateRandomWords(labelLength, ""))
		}

		opts := goGitLab.CreateIssueOptions{
			Title:       goGitLab.String(issueTitle),
			Description: goGitLab.String(issueContent),
			Labels:      &issueLabels,
		}

		issue, _, err := args.GitLabClient.Issues.CreateIssue(project.ID, &opts, goGitLab.WithContext(args.Context))
		if err != nil {
			level.Error(args.Logger).Log("event", "CreateIssue", "project_weburl", project.WebURL,
				"issue_title", issueTitle, "msg", "failed to create issue", "error", err)
			continue
		}

		level.Info(args.Logger).Log("event", "CreateIssue", "project_weburl", project.WebURL,
			"issue_title", issueTitle, "issue_weburl", issue.WebURL, "msg", "issue created")
		CreateComments(args, project, issue)
	}
}
