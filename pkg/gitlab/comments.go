package gitlab

import (
	"github.com/go-kit/log/level"
	"github.com/mroth/weightedrand/v2"
	goGitLab "github.com/xanzy/go-gitlab"
)

type CommentsExtraOptions struct {
	CommentsOnlyFileUpload bool `yaml:"comments_only_file_upload"`
	UploadFileSizeMB       int  `yaml:"upload_file_size_mb"`
}

const (
	maxCommentLength    = 50
	maxCommentsToCreate = 25
	minCommentLength    = 10
	minCommentsToCreate = 3
)

func CreateComments(args *Args, project *goGitLab.Project, issue *goGitLab.Issue) {
	const (
		commentTypeImage = "image"
		commentTypeFile  = "file"
		commentTypeText  = "text"
	)

	level.Info(args.Logger).Log("event", "CreateComment", "issue_weburl", issue.WebURL)
	commentCounts := countConfig{
		ItemsToCreate:    0,
		MinItemsToCreate: minCommentsToCreate,
		MaxItemsToCreate: maxCommentsToCreate,
	}
	commentLengths := countConfig{
		ItemsToCreate:    0,
		MinItemsToCreate: minCommentLength,
		MaxItemsToCreate: maxCommentLength,
	}

	for i := 0; i < itemsToCreate(commentCounts); i++ {
		var (
			body string
			err  error
		)

		waitForEnvironmentHealthy()

		commentTypes, _ := weightedrand.NewChooser(
			weightedrand.NewChoice(commentTypeImage, 3),
			weightedrand.NewChoice(commentTypeFile, 3),
			weightedrand.NewChoice(commentTypeText, 7),
		)
		commentType := commentTypes.Pick()

		switch {
		case commentType == commentTypeFile || args.Config.ExtraOptions.Comments.CommentsOnlyFileUpload:
			body, err = uploadFile(args, project)
			if err != nil {
				level.Error(args.Logger).Log("event", "CreateCommentFile", "msg", "failed to create file", "error", err)
				continue
			}
		case commentType == commentTypeImage:
			body, err = uploadImage(args, project)
			if err != nil {
				level.Error(args.Logger).Log("event", "CreateCommentImage", "msg", "failed to create image", "error", err)
				continue
			}
		case commentType == commentTypeText:
			body = generateRandomWords(itemsToCreate(commentLengths), " ")
		}

		opts := goGitLab.CreateIssueDiscussionOptions{
			Body: goGitLab.String(body),
		}

		_, _, err = args.GitLabClient.Discussions.CreateIssueDiscussion(project.ID, issue.IID, &opts, goGitLab.WithContext(args.Context))
		if err != nil {
			level.Error(args.Logger).Log("event", "CreateComment", "msg", "failed to create comment", "error", err)
			continue
		}

		level.Info(args.Logger).Log("event", "CreateComment", "issue_weburl", issue.WebURL, "msg", "comment created")
	}
}
