package gitlab

import (
	"fmt"
	"math/rand"

	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	branchNameLength    = 3
	commitMessageLength = 3
	startBranchName     = "main"
)

func CreateBranch(args *Args, project *goGitLab.Project) {
	if !args.Config.Project.Repository.Create {
		return
	}

	branchCount := itemsToCreate(args.Config.Project.Repository.Branches)
	level.Info(args.Logger).Log("event", "CreateBranch", "branch_count", branchCount, "project_weburl", project.WebURL)

	for i := 0; i < branchCount; i++ {
		waitForEnvironmentHealthy()

		branchName := generateRandomWords(branchNameLength, "_")
		fileName := fmt.Sprintf("%v.%v", generateRandomWords(fileNameLength, ""), fileExtensions[rand.Intn(len(fileExtensions))])
		opts := goGitLab.CreateFileOptions{
			Branch:        goGitLab.String(branchName),
			StartBranch:   goGitLab.String(startBranchName),
			CommitMessage: goGitLab.String(fmt.Sprintf("Commit - %v", generateRandomWords(commitMessageLength, " "))),
			Content:       goGitLab.String(generateFileContent(fileContentMinNumberOfLines, fileContentMaxNumberOfLines)),
		}

		_, _, err := args.GitLabClient.RepositoryFiles.CreateFile(project.ID, fileName, &opts, goGitLab.WithContext(args.Context))
		if err != nil {
			level.Error(args.Logger).Log("event", "CreateBranch", "project_weburl", project.WebURL,
				"branch_name", branchName, "msg", "failed to create branch", "error", err)
			return
		}

		level.Info(args.Logger).Log("event", "CreateBranch", "project_weburl", project.WebURL,
			"branch_name", branchName, "msg", "branch created")
		CreateMergeRequest(args, project, branchName)
	}
}
