package gitlab

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/go-kit/log/level"
	cp "github.com/otiai10/copy"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	packageNameLength = 3
)

func UploadPackage(args *Args, project *goGitLab.Project, apiToken string) {
	level.Info(args.Logger).Log("event", "UploadPyPackage", "project_weburl", project.WebURL)
	parentTmpDir, err := os.MkdirTemp("./assets/packages/", "builds")
	if err != nil {
		return
	}
	defer func() { _ = os.RemoveAll(parentTmpDir) }()

	for i := 0; i < itemsToCreate(args.Config.Project.Assets.PyPackages); i++ {
		waitForEnvironmentHealthy()

		tmpDir, err := os.MkdirTemp(parentTmpDir, "python")
		if err != nil {
			continue
		}

		err = updatePyProject(tmpDir)
		if err != nil {
			continue
		}

		level.Info(args.Logger).Log("event", "pythonBuild", "project_weburl", project.WebURL)
		buildCmd := exec.Command("python3", "-m", "build", tmpDir, "--wheel")
		_, buildErr := buildCmd.Output()
		if buildErr != nil {
			level.Error(args.Logger).Log("event", "pythonBuild", "project_weburl", project.WebURL,
				"msg", "failed to build package", "error", buildErr)
			continue
		}

		level.Info(args.Logger).Log("event", "pythonPublish", "project_weburl", project.WebURL)
		pushCmd := exec.Command("python3", "-m", "twine", "upload", "--repository-url",
			fmt.Sprintf("%v/projects/%v/packages/pypi", args.Config.GitLab.APIURL, project.ID), fmt.Sprintf("%v/dist/*", tmpDir))
		pushCmd.Env = os.Environ()
		pushCmd.Env = append(pushCmd.Env, "TWINE_USERNAME=root")
		pushCmd.Env = append(pushCmd.Env, fmt.Sprintf("TWINE_PASSWORD=%v", apiToken))
		_, pushErr := pushCmd.Output()
		if pushErr != nil {
			level.Error(args.Logger).Log("event", "pythonPublish", "project_weburl", project.WebURL,
				"msg", "failed to publish package", "error", pushErr)
			continue
		}

		level.Info(args.Logger).Log("event", "UploadContainer", "project_weburl", project.WebURL, "msg", "Container Uploaded")
	}
}

func updatePyProject(tmpDir string) error {
	packageName := fmt.Sprintf("pypipackage-%v", generateRandomWords(packageNameLength, "-"))

	err := cp.Copy("./assets/packages/python/pypipackage", fmt.Sprintf("%v/pypipackage", tmpDir))
	if err != nil {
		return err
	}

	input, err := os.ReadFile("./assets/packages/python/pyproject.toml")
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")

	for i, line := range lines {
		if strings.Contains(line, "name = ") {
			lines[i] = fmt.Sprintf("name = \"%v\"", packageName)
		}
	}
	output := strings.Join(lines, "\n")
	err = os.WriteFile(fmt.Sprintf("%v/pyproject.toml", tmpDir), []byte(output), 0600)
	if err != nil {
		return err
	}

	return nil
}
