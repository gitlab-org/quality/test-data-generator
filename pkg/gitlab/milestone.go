package gitlab

import (
	"context"
	"fmt"
	"time"

	"github.com/go-kit/log/level"
	"github.com/pkg/errors"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	milestoneNameLength = 3
)

func CreateGroupMilestone(ctx context.Context, args *Args, group *goGitLab.Group) []func() error {
	level.Info(args.Logger).Log("event", "CreateGroupMilestone", "group_weburl", group.WebURL)

	return createMilestone(ctx, args, group, nil)
}

func CreateProjectMilestone(ctx context.Context, args *Args, project *goGitLab.Project) []func() error {
	level.Info(args.Logger).Log("event", "CreateProjectMilestone", "project_weburl", project.WebURL)

	return createMilestone(ctx, args, nil, project)
}

func createMilestone(ctx context.Context, args *Args, group *goGitLab.Group, project *goGitLab.Project) []func() error {

	var items []func() error

	if project != nil {
		milestonesToCreate := itemsToCreate(args.Config.Project.Assets.Milestones)
		for i := 0; i <= milestonesToCreate; i++ {
			items = append(items, createProjectMilestone(ctx, args, project))
		}
	}

	if group != nil {
		milestonesToCreate := itemsToCreate(args.Config.ParentGroup.Assets.Milestones)
		for i := 0; i <= milestonesToCreate; i++ {
			items = append(items, createGroupMilestone(ctx, args, group))
		}
	}

	return items
}

func createProjectMilestone(ctx context.Context, args *Args, p *goGitLab.Project) func() error {
	opts := goGitLab.CreateMilestoneOptions{
		Title:       goGitLab.String(fmt.Sprintf("milestone-%v", generateRandomWords(milestoneNameLength, "-"))),
		Description: goGitLab.String(fmt.Sprint("Created via Test Data Generator", generateRandomWords(milestoneNameLength, " "))),
		DueDate:     (*goGitLab.ISOTime)(goGitLab.Time(time.Now().Add(240 * time.Hour))),
		StartDate:   (*goGitLab.ISOTime)(goGitLab.Time(time.Now())),
	}

	return func() error {
		m, _, err := args.GitLabClient.Milestones.CreateMilestone(p.ID, &opts, goGitLab.WithContext(ctx))
		if err != nil {
			level.Error(args.Logger).Log("event", "CreateGroupMilestone", "project_weburl", p.WebURL,
				"milestone_title", *opts.Title, "msg", "failed to create group milestone", "error", err)
			return errors.WithMessage(err, "failed to create project milestone")
		}

		level.Info(args.Logger).Log("event", "CreateProjectMilestone", "milestone_name", *opts.Title,
			"milestone_iid", m.IID, "msg", "project milestone created")

		return nil
	}
}

func createGroupMilestone(ctx context.Context, args *Args, g *goGitLab.Group) func() error {
	opts := goGitLab.CreateGroupMilestoneOptions{
		Title:       goGitLab.String(fmt.Sprintf("milestone-%v", generateRandomWords(milestoneNameLength, "-"))),
		Description: goGitLab.String(fmt.Sprint("Created via Test Data Generator", generateRandomWords(milestoneNameLength, " "))),
		DueDate:     (*goGitLab.ISOTime)(goGitLab.Time(time.Now().Add(240 * time.Hour))),
		StartDate:   (*goGitLab.ISOTime)(goGitLab.Time(time.Now())),
	}

	return func() error {
		m, _, err := args.GitLabClient.GroupMilestones.CreateGroupMilestone(g.ID, &opts, goGitLab.WithContext(ctx))
		if err != nil {
			level.Error(args.Logger).Log("event", "CreateGroupMilestone", "group_weburl", g.WebURL,
				"milestone_title", *opts.Title, "msg", "failed to create group milestone", "error", err)
			return errors.WithMessage(err, "Failed to create group milestone")
		}

		level.Info(args.Logger).Log("event", "CreateGroupMilestone", "milestone_name", *opts.Title,
			"milestone_iid", m.IID, "msg", "group milestone created")

		return nil
	}
}
