package gitlab

import (
	"fmt"
	"math/rand"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	wikiTitleLength = 3
)

func CreateProjectWiki(args *Args, project *goGitLab.Project) {
	wikiLog := log.With(args.Logger, "event", "CreateProjectWiki", "project_weburl", project.WebURL)
	createWiki(args, wikiLog, args.Config.Project.Assets.Wikis, project, nil)
}

func CreateGroupWiki(args *Args, group *goGitLab.Group) {
	wikiLog := log.With(args.Logger, "event", "CreateGroupWiki", "group_weburl", group.WebURL)
	createWiki(args, wikiLog, args.Config.ParentGroup.Assets.Wikis, nil, group)
}

func createWiki(args *Args, wikiLog log.Logger, wikiCfg countConfig, project *goGitLab.Project, group *goGitLab.Group) {
	wikiFormats := []goGitLab.WikiFormatValue{"markdown", "rdoc", "asciidoc", "org"}
	wikisToCreate := itemsToCreate(wikiCfg)
	level.Info(wikiLog).Log("wiki_count", wikisToCreate)

	for i := 0; i < wikisToCreate; i++ {
		waitForEnvironmentHealthy()

		wikiTitle := fmt.Sprintf("wiki-%v", generateRandomWords(wikiTitleLength, "-"))
		wikiContent := generateFileContent(fileContentMinNumberOfLines, fileContentMaxNumberOfLines)
		var err error

		if project != nil {
			opts := goGitLab.CreateWikiPageOptions{
				Title:   goGitLab.String(wikiTitle),
				Format:  &wikiFormats[rand.Intn(len(wikiFormats))],
				Content: goGitLab.String(wikiContent),
			}
			_, _, err = args.GitLabClient.Wikis.CreateWikiPage(project.ID, &opts, goGitLab.WithContext(args.Context))
		} else {
			opts := goGitLab.CreateGroupWikiPageOptions{
				Title:   goGitLab.String(wikiTitle),
				Format:  &wikiFormats[rand.Intn(len(wikiFormats))],
				Content: goGitLab.String(wikiContent),
			}
			_, _, err = args.GitLabClient.GroupWikis.CreateGroupWikiPage(group.ID, &opts, goGitLab.WithContext(args.Context))
		}

		if err != nil {
			level.Error(wikiLog).Log("wiki_name", wikiTitle, "msg", "failed to create wiki", "error", err)
			continue
		}

		level.Info(wikiLog).Log("wiki_name", wikiTitle, "msg", "wiki created")
	}
}
