package gitlab

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

type RepositoryCfg struct {
	Create      bool        `yaml:"create"`
	CICD        bool        `yaml:"cicd"`
	Branches    countConfig `yaml:"branches"`
	Dirs        countConfig `yaml:"dirs"`
	FilesPerDir countConfig `yaml:"files_per_dir"`
	LFS         countConfig `yaml:"lfs"`
}

const (
	authorNameLength            = 2
	dirNameLength               = 1
	fileContentMaxNumberOfLines = 1000
	fileContentMinNumberOfLines = 100
	fileNameLength              = 1
)

var fileExtensions = []string{"rb", "txt", "py", "md", "pl", "sh", "php", "java", "js", "html", "cs", "tf"}

func CreateRepository(args *Args, project *goGitLab.Project, apiToken string) error {
	if !args.Config.Project.Repository.Create {
		return nil
	}

	tmpDir, err := os.MkdirTemp("", "tdg")
	if err != nil {
		level.Error(args.Logger).Log("event", "createTmpDir", "tmp_dir", tmpDir, "msg", "failed to create tmp dir")
		return err
	}
	level.Info(args.Logger).Log("event", "createTmpDir", "tmp_dir", tmpDir)

	repo, err := cloneRepository(project, args.Logger, tmpDir, apiToken)
	if err != nil {
		return err
	}

	dirs, err := createSubDirs(args, project, tmpDir)
	if err != nil {
		return err
	}

	err = createFiles(args, project, repo, dirs)
	if err != nil {
		return err
	}

	err = pushRepository(project, repo, args.Logger, apiToken)
	if err != nil {
		return err
	}

	err = deleteTmpDir(args.Logger, tmpDir)
	if err != nil {
		return err
	}

	err = addLFSFiles(args, project)
	if err != nil {
		return err
	}

	return nil
}

func cloneRepository(project *goGitLab.Project, logger log.Logger, tmpDir string, apiToken string) (*git.Repository, error) {
	level.Info(logger).Log("event", "cloneRepository", "tmp_dir", tmpDir, "project_weburl", project.WebURL)
	repo, err := git.PlainClone(tmpDir, false, &git.CloneOptions{
		URL:      project.HTTPURLToRepo,
		Progress: os.Stdout,
		Auth: &http.BasicAuth{
			Username: "abc",
			Password: apiToken,
		},
		InsecureSkipTLS: true,
	})
	if err != nil {
		level.Error(logger).Log("event", "cloneRepository", "tmp_dir", tmpDir, "project_weburl", project.WebURL,
			"msg", "failed to clone repository", "error", err)
		return nil, err
	}
	return repo, nil
}

func createSubDirs(args *Args, project *goGitLab.Project, tmpDir string) ([]string, error) {
	dirsToCreate := itemsToCreate(args.Config.Project.Repository.Dirs)

	subDirLog := log.With(args.Logger, "event", "createSubDirs", "tmp_dir", tmpDir, "project_weburl", project.WebURL,
		"dirs_to_create", dirsToCreate)
	level.Info(subDirLog).Log()

	dirs := []string{tmpDir}
	for i := 0; i < dirsToCreate; i++ {
		dirName := fmt.Sprintf("%v/%v", dirs[rand.Intn(len(dirs))], generateRandomWords(dirNameLength, ""))
		err := os.Mkdir(dirName, 0755)
		if err != nil {
			level.Error(subDirLog).Log("dirs_created", i-1, "dir_name", dirName, "msg", "failed to create all directories", "error", err)
			return []string{}, err
		}

		dirs = append(dirs, dirName)
	}

	level.Info(subDirLog).Log("msg", "created all sub directories")
	return dirs, nil
}

func createFiles(args *Args, project *goGitLab.Project, repo *git.Repository, dirs []string) error {
	fileLog := log.With(args.Logger, "event", "createFiles", "project_weburl", project.WebURL)
	level.Info(fileLog).Log()

	for _, dirName := range dirs {
		filesToCreate := itemsToCreate(args.Config.Project.Repository.FilesPerDir)
		for i := 0; i < filesToCreate; i++ {
			var err error
			level.Info(fileLog).Log("dir", dirName, "files_to_create", filesToCreate)
			fileName := fmt.Sprintf("%v.%v", generateRandomWords(fileNameLength, ""), fileExtensions[rand.Intn(len(fileExtensions))])
			filePath := fmt.Sprintf("%v/%v", dirName, fileName)

			if rand.Intn(10000)%3 == 0 {
				err = os.WriteFile(filePath, generateLargeFileData(args.Config.ExtraOptions.Comments.UploadFileSizeMB), 0644)
			} else {
				err = os.WriteFile(filePath, []byte(generateFileContent(fileContentMinNumberOfLines, fileContentMaxNumberOfLines)), 0644)
			}
			if err != nil {
				level.Error(fileLog).Log("files_created", i-1, "file_path", filePath, "msg", "failed to create file", "error", err)
				return err
			}

			err = commitFile(project, repo, args.Logger, dirs[0], filePath)
			if err != nil {
				return err
			}
		}
	}

	if args.Config.Project.Repository.CICD {
		err := addCICDFile(args, project, repo, dirs[0], fileLog)
		if err != nil {
			return err
		}
	}

	level.Info(fileLog).Log("msg", "finished creating files")
	return nil
}

func addCICDFile(args *Args, project *goGitLab.Project, repo *git.Repository, repoDir string, fileLog log.Logger) error {
	const (
		sourceFilePath = "assets/cicd/.gitlab-ci.yml"
	)
	fileName := ".gitlab-ci.yml"
	filePath := fmt.Sprintf("%v/%v", repoDir, fileName)

	source, err := os.Open(sourceFilePath)
	if err != nil {
		level.Error(fileLog).Log("file_path", sourceFilePath, "msg", "failed to open file", "error", err)
		return err
	}
	defer source.Close()

	destination, err := os.Create(filePath)
	if err != nil {
		level.Error(fileLog).Log("file_path", filePath, "msg", "failed to create file", "error", err)
		return err
	}
	defer destination.Close()

	_, err = io.Copy(destination, source)
	if err != nil {
		level.Error(fileLog).Log("source", sourceFilePath, "destination", filePath, "msg", "failed to copy file", "error", err)
		return err
	}

	err = commitFile(project, repo, args.Logger, repoDir, filePath)
	if err != nil {
		return err
	}

	return nil
}

func commitFile(project *goGitLab.Project, repo *git.Repository, logger log.Logger, tmpDir string, filePath string) error {
	commitLog := log.With(logger, "event", "commitFile", "project_weburl", project.WebURL, "file_path", filePath)
	workTree, err := repo.Worktree()
	if err != nil {
		level.Error(commitLog).Log("msg", "git worktree failure")
		return err
	}

	fileName := strings.ReplaceAll(filePath, tmpDir+"/", "")
	_, err = workTree.Add(fileName)
	if err != nil {
		level.Error(commitLog).Log("msg", "git add failure")
		return err
	}

	commit, _ := workTree.Commit(fmt.Sprintf("Add file: %v", fileName), &git.CommitOptions{
		Author: &object.Signature{
			Name:  generateRandomWords(authorNameLength, " "),
			Email: generateRandomWords(authorNameLength, "@") + ".com",
			When:  time.Now(),
		},
	})

	_, err = repo.CommitObject(commit)
	if err != nil {
		level.Error(commitLog).Log("msg", "git commit failure", "error", err)
		return err
	}

	return nil
}

func pushRepository(project *goGitLab.Project, repo *git.Repository, logger log.Logger, apiToken string) error {
	pushLog := log.With(logger, "event", "pushRepository", "project_weburl", project.WebURL)
	level.Info(pushLog).Log()

	waitForEnvironmentHealthy()
	err := repo.Push(&git.PushOptions{
		RemoteURL: project.HTTPURLToRepo,
		Progress:  os.Stdout,
		Auth: &http.BasicAuth{
			Username: "abc",
			Password: apiToken,
		},
		InsecureSkipTLS: true,
	})
	if err != nil {
		level.Error(pushLog).Log("msg", "failed to push repository", "error", err)
		return err
	}

	level.Info(pushLog).Log("msg", "finished pushing repository")
	return nil
}

type LFSExtraOptions struct {
	FileSizeMB int `yaml:"file_size_mb"`
}

func addLFSFiles(args *Args, project *goGitLab.Project) error {
	const (
		lfsExtension         = "bin"
		gitAttributesContent = "*.bin filter=lfs diff=lfs merge=lfs -text"
	)

	tmpDir, err := os.MkdirTemp("", "tdg")
	if err != nil {
		level.Error(args.Logger).Log("event", "createLFSTmpDir", "tmp_dir", tmpDir, "msg", "failed to create tmp dir")
		return err
	}
	level.Info(args.Logger).Log("event", "createLFSTmpDir", "tmp_dir", tmpDir)

	itemsToCreate := itemsToCreate(args.Config.Project.Repository.LFS)
	if itemsToCreate == 0 {
		return nil
	}

	waitForEnvironmentHealthy()

	level.Info(args.Logger).Log("event", "lfsClone", "project_weburl", project.WebURL)
	cloneCmd := exec.Command("git", "clone", "--depth", "1", project.SSHURLToRepo, tmpDir)
	_, err = cloneCmd.Output()
	if err != nil {
		level.Error(args.Logger).Log("event", "lfsClone", "project_weburl", project.WebURL,
			"msg", "failed to clone repository", "error", err)
		return err
	}

	lfsDir := fmt.Sprintf("%v/lfs", tmpDir)
	err = os.Mkdir(lfsDir, 0755)
	if err != nil {
		return err
	}

	gitAttributesPath := fmt.Sprintf("%v/.gitattributes", lfsDir)
	err = os.WriteFile(gitAttributesPath, []byte(gitAttributesContent), 0644)
	if err != nil {
		level.Error(args.Logger).Log("event", "createLFSFiles", "msg", "failed to create .gitattributes", "error", err)
		return err
	}

	gitCommit(args, project, tmpDir)

	for i := 0; i < itemsToCreate; i++ {
		fileName := fmt.Sprintf("%v.%v", generateRandomWords(fileNameLength, ""), lfsExtension)
		filePath := fmt.Sprintf("%v/%v", lfsDir, fileName)

		err = os.WriteFile(filePath, generateLargeFileData(args.Config.ExtraOptions.LFS.FileSizeMB), 0600)
		if err != nil {
			level.Error(args.Logger).Log("event", "createLFSFile", "file_path", filePath, "msg", "failed to create file", "error", err)
		}
	}

	gitCommit(args, project, tmpDir)

	waitForEnvironmentHealthy()
	level.Info(args.Logger).Log("event", "lfsPush", "project_weburl", project.WebURL)
	pushCmd := exec.Command("git", "push")
	pushCmd.Dir = tmpDir
	_, err = pushCmd.Output()
	if err != nil {
		return err
	}

	_ = os.RemoveAll(tmpDir)

	return nil
}

func gitCommit(args *Args, project *goGitLab.Project, repoDir string) {
	level.Info(args.Logger).Log("event", "lfsAdd", "project_weburl", project.WebURL)
	addCmd := exec.Command("git", "add", ".")
	addCmd.Dir = repoDir
	_, _ = addCmd.Output()

	level.Info(args.Logger).Log("event", "lfsCommit", "project_weburl", project.WebURL)
	commitCmd := exec.Command("git", "commit", "-m", "Add LFS files")
	commitCmd.Dir = repoDir
	_, _ = commitCmd.Output()
}

func deleteTmpDir(logger log.Logger, tmpDir string) error {
	if err := os.RemoveAll(tmpDir); err != nil {
		level.Error(logger).Log("event", "deleteTmpDir", "dir", tmpDir, "msg", "failed to delete dir", "error", err)
		return err
	}
	return nil
}
