package gitlab

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"net/http"

	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func uploadImage(args *Args, project *goGitLab.Project) (string, error) {
	resp, err := http.Get("https://picsum.photos/800")
	if err != nil {
		return "", err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	imageName := fmt.Sprintf("%v.jpg", generateRandomWords(2, "-"))

	waitForEnvironmentHealthy()

	upload, _, err := args.GitLabClient.Projects.UploadFile(project.ID, resp.Body,
		imageName, goGitLab.WithContext(args.Context))
	if err != nil {
		level.Error(args.Logger).Log("event", "UploadImage", "project_weburl", project.WebURL,
			"msg", "failed to upload image", "error", err)
		return "", err
	}

	return upload.Markdown, nil
}

func uploadFile(args *Args, project *goGitLab.Project) (string, error) {
	data := generateLargeFileData(args.Config.ExtraOptions.Comments.UploadFileSizeMB)
	// Fill the byte slice with random data
	_, err := rand.Read(data)
	if err != nil {
		level.Error(args.Logger).Log("event", "UploadFile", "project_weburl", project.WebURL,
			"msg", "failed to generate random data", "error", err)
		return "", err
	}

	fileName := fmt.Sprintf("%v.txt", generateRandomWords(2, "-"))
	dataReader := bytes.NewReader(data)

	waitForEnvironmentHealthy()

	upload, _, err := args.GitLabClient.Projects.UploadFile(project.ID, dataReader,
		fileName, goGitLab.WithContext(args.Context))
	if err != nil {
		level.Error(args.Logger).Log("event", "UploadFile", "project_weburl", project.WebURL,
			"msg", "failed to upload file", "error", err)
		return "", err
	}

	return upload.Markdown, nil
}
