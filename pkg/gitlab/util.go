package gitlab

import (
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

const (
	allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	maxLineLength = 80
	maxWordLength = 12
	minLineLength = 30
	minWordLength = 5

	oneMB  = 1048576
	fiveMB = 5242880
)

func generateRandomWords(numberOfWords int, spacer string) string {
	s := ""

	for i := 1; i <= numberOfWords; i++ {
		s += randomString(minWordLength + rand.Intn(maxWordLength-minWordLength))
		if i < numberOfWords {
			s += spacer
		}
	}
	return s
}

func generateFileContent(minNumberOfLines int, maxNumberOfLines int) string {
	s := ""

	if maxNumberOfLines <= 0 {
		return "Max number of lines cannot be zero"
	} else if minNumberOfLines > maxNumberOfLines {
		return "Min number of lines cannot be greater than max number of lines"
	}

	lines := minNumberOfLines + rand.Intn(maxNumberOfLines-minNumberOfLines)
	for i := 0; i < lines; i++ {
		lineLength := minLineLength + rand.Intn(maxLineLength-minLineLength)
		line := ""
		if lineLength >= 40 {
			for len(line) < lineLength {
				line += randomString(minWordLength+rand.Intn(maxWordLength-minWordLength)) + " "
			}
		}
		s += line + "\n"
	}

	tmpString := strings.ReplaceAll(s, "\n", "")
	if tmpString == "" {
		s = "Something went wrong!"
	}

	return s
}

func generateLargeFileData(largeFileDataSize int) []byte {
	if largeFileDataSize != 0 {
		return make([]byte, largeFileDataSize*oneMB)
	}
	return make([]byte, oneMB+rand.Intn(fiveMB-oneMB))
}

func randomString(length int) string {
	b := make([]byte, length)
	for i := 0; i < length; i++ {
		b[i] = allowedChars[rand.Intn(len(allowedChars))]
	}
	return string(b)
}

func itemsToCreate(counts countConfig) int {
	if counts.MaxItemsToCreate == 0 && counts.ItemsToCreate == 0 {
		return 0
	} else if counts.ItemsToCreate > 0 {
		return counts.ItemsToCreate
	}

	if counts.MaxItemsToCreate <= counts.MinItemsToCreate {
		return counts.MinItemsToCreate
	}

	return rand.Intn(counts.MaxItemsToCreate-counts.MinItemsToCreate) + counts.MinItemsToCreate
}

var (
	environmentHealthy = true
)

func MonitorEnvironmentHealth(gitlabURL string) {
	for {
		resp, err := http.Get(fmt.Sprintf("%v/-/health", gitlabURL))
		if resp != nil {
			_ = resp.Body.Close()
		}
		environmentHealthy = err == nil && resp != nil && resp.StatusCode == http.StatusOK
		time.Sleep(5 * time.Second)
	}
}

func waitForEnvironmentHealthy() {
	for {
		if environmentHealthy {
			return
		}

		fmt.Println("Waiting for environment to become healthy...")
		time.Sleep(time.Second)
	}
}
