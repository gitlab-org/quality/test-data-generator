package gitlab

import (
	"context"
	"fmt"
	"time"

	"github.com/go-kit/log"
	goGitLab "github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v3"
)

type Config struct {
	GitLab        endpointsConfig     `yaml:"gitlab"`
	ParentGroup   ParentGroupCfg      `yaml:"parent_group"`
	Project       ProjectCfg          `yaml:"project"`
	Concurrencies concurrenciesConfig `yaml:"concurrencies"`
	ExtraOptions  extraOptionsCfg     `yaml:"extra_options"`
}

type endpointsConfig struct {
	URL                  string `yaml:"-"`
	APIURL               string `yaml:"-"`
	ContainerRegistryURL string `yaml:"-"`
}

type assetsConfig struct {
	Issues     countConfig `yaml:"issues"`
	Snippets   countConfig `yaml:"snippets"`
	Wikis      countConfig `yaml:"wikis"`
	Containers countConfig `yaml:"containers"`
	PyPackages countConfig `yaml:"py_packages"`
	Milestones countConfig `yaml:"milestones"`
}

type countConfig struct {
	ItemsToCreate    int `yaml:"items_to_create"`
	MinItemsToCreate int `yaml:"min_items_to_create"`
	MaxItemsToCreate int `yaml:"max_items_to_create"`
}

type Args struct {
	Context      context.Context
	GitLabClient *goGitLab.Client
	Config       *Config
	Logger       log.Logger
}

type concurrenciesConfig struct {
	Snippets        int64 `yaml:"snippets"`
	GroupWikis      int64 `yaml:"group_wikis"`
	Projects        int64 `yaml:"projects"`
	Branches        int64 `yaml:"branches"`
	ProjectSnippets int64 `yaml:"project_snippets"`
	ProjectWikis    int64 `yaml:"project_wikis"`
	Issues          int64 `yaml:"issues"`
}

type extraOptionsCfg struct {
	Comments CommentsExtraOptions `yaml:"comments"`
	LFS      LFSExtraOptions      `yaml:"lfs"`
}

func LoadConfig(data []byte, gitlabURL string, gitlabRegistryURL string) (*Config, error) {
	const (
		defaultMinCount        = 3
		defaultConcurrency     = 1
		defaultLargeFileSizeMB = 2
	)

	defaultCountCfg := countConfig{
		MinItemsToCreate: defaultMinCount,
	}

	cfg := &Config{
		ParentGroup: ParentGroupCfg{
			Assets: assetsConfig{
				Snippets: defaultCountCfg,
				Wikis:    defaultCountCfg,
			},
		},
		Project: ProjectCfg{
			Repository: RepositoryCfg{
				Dirs:        defaultCountCfg,
				FilesPerDir: defaultCountCfg,
			},
			Assets: assetsConfig{
				Issues:   defaultCountCfg,
				Snippets: defaultCountCfg,
				Wikis:    defaultCountCfg,
			},
			RunTime: time.Second,
		},
		Concurrencies: concurrenciesConfig{
			Snippets:        defaultConcurrency,
			GroupWikis:      defaultConcurrency,
			Projects:        defaultConcurrency,
			Branches:        defaultConcurrency,
			ProjectSnippets: defaultConcurrency,
			ProjectWikis:    defaultConcurrency,
			Issues:          defaultConcurrency,
		},
		ExtraOptions: extraOptionsCfg{
			Comments: CommentsExtraOptions{
				CommentsOnlyFileUpload: false,
				UploadFileSizeMB:       defaultLargeFileSizeMB,
			},
			LFS: LFSExtraOptions{
				FileSizeMB: defaultLargeFileSizeMB,
			},
		},
	}

	err := yaml.Unmarshal(data, &cfg)
	if err != nil {
		return &Config{}, err
	}

	cfg.GitLab.URL = gitlabURL
	cfg.GitLab.ContainerRegistryURL = gitlabRegistryURL
	cfg.GitLab.APIURL = fmt.Sprintf("%v/api/v4", gitlabURL)

	return cfg, nil
}
