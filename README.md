# Test Data Generator

The Test Data Generator is designed to populate a GitLab instance with dummy data that can be used to simulate a larger production instance.
The generated data is intended to be random and as such, each project created should be "unique". All data is created under a test `Group`
within GitLab to help with clean-up, however, it is advised that this tool should not be run against a production environment.

[[_TOC_]]

## Generated Data

The below list shows the currently available data that can be generated, as well as the structure used when creating data.
You can read more on how to configure each data type by clicking into the individual data types listed below:

- [Groups](./docs/groups.md)
  - [Projects](./docs/projects.md)
    - [Issues](./docs/issues.md)
      - [Comments](./docs/issues.md#comments) with random uploads
    - [Repositories](./docs/repositories.md)
      - [Branches](./docs/branches.md)
        - [Merge Requests](./docs/branches.md#merge-requests)
      - [Pipelines](./docs/pipelines.md)
      - [LFS](./docs/repositories.md)
    - [Snippets](./docs/snippets.md)
    - [Wikis](./docs/wikis.md)
    - Container Registry
    - Package Registry
  - [Wikis](./docs/wikis.md)
- [Snippets](./docs/snippets.md)

## Configuration

To configure the Test Data Generator, create a `yml` config file in `./configs/` to specify how much of each data
type should be created. An example file is available in `./configs/example.yml`.

```yaml
# Settings for the parent group
parent_group:
  name: test-data-1
  assets:
    snippets:
      max_items_to_create: 10
    wikis:
      max_items_to_create: 30

# Settings for the amount of data to be created for each project
project:
  concurrency: 2
  count: 10
  repository:
    create: true
    cicd: true
    branches:
      max_items_to_create: 15
    dirs:
      min_items_to_create: 10
      max_items_to_create: 15
    files_per_dir:
      min_items_to_create: 100
      max_items_to_create: 150
    lfs:
      min_items_to_create: 1
      max_items_to_create: 10
  assets:
    snippets:
      max_items_to_create: 5
    wikis:
      max_items_to_create: 10
    issues:
      max_items_to_create: 10
```

A concept that runs throughout the Test Data Generator is the ability to specify a specific amount of an asset to create or a
random amount. Generating a specific amount can be useful if you're wanting to use the Test Data Generator to create a single
large project that has large amounts of any specific assets. The ability to generate random amounts of each asset type is more
useful when generating large amounts of projects, using random numbers will allow each project to be different and aims to create
a more realistic split of data across a GitLab instance. The below settings are available on each asset type to generate a set
amount of data or to generate a random amount:

- `items_to_create`: If set, the specified number will be created of that asset type. Optional, if you want to create any amount
of an asset, either `items_to_create` or `max_items_to_create` must be specified.
- `min_items_to_create`: When creating a random amount for an asset you can specify a minimum amount to create. Optional, Defaults to 3.
- `max_items_to_create`: To create a random amount of an asset, you must specify the upper limit. A random number will then be
generated between the min & max numbers specified. Optional, if you want to create any amount of an asset, either `items_to_create`
or `max_items_to_create` must be specified.

## Running the tool

```text
Flags:
  --[no-]help                  Show context-sensitive help (also try --help-long and --help-man).
  --config=CONFIG              Path to config file ($TDG_CONFIG)
  --gitlab-token=GITLAB-TOKEN  Admin API token ($TDG_API_TOKEN)
  --gitlab-url=GITLAB-URL      URL for the GitLab instance to seed. ($TDG_GITLAB_URL)
  --gitlab-registry-url=GITLAB-REGISTRY-URL  
                               Registry URL for the GitLab instance to seed. ($TDG_GITLAB_REGISTRY_URL)
```

The Test Data Generator is written in Go, to run the tool run:

```sh
go run ./cmd/tdg create --config ./configs/<your config>.yml --gitlab-token <GitLab API token> --gitlab-url <GitLab URL>
```

## ToDo

Upcoming new assets types to be added to the generator

- Repository Forks
- Milestones
- Users

Please feel free to raise an issue/MR with any asset types that could also be added or for any general improvements.
